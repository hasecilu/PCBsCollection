# Simple PCBs collection

Some small PCBs created using [KiCad EDA](https://www.kicad.org/) version 6.0

## Encoder

Small encoder for a Faulhaber motor

## 2x2x2 RGB LED CUBE

Small LED cube made just for fun.

Go to [this repo](https://gitlab.com/hasecilu/2x2x2-rgb-led-cube)

## 16x16 LED matrix

This is a simple PCB made in KiCad using 4 small 8x8 LED matrices serially connected to form a 16x16 LED matrix.

Go to [this repo](https://gitlab.com/hasecilu/16x16-led-matrix)

## 8x8 LED matrix old school

A simple 8x8 LED matrix without any special driver.

## VGA

This is a simple PCB made in KiCad for my FPGA.

Go to [this repo](https://gitlab.com/hasecilu/VGA)

## LED

Various PCB using LED for prototyping purposes.

## DIP switch

DIP switch board with 8 switches
